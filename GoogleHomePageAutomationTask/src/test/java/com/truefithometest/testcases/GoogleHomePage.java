package com.truefithometest.testcases;

import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.JavascriptExecutor;

import com.truefithometest.base.TestBase;

public class GoogleHomePage extends TestBase {

	SoftAssert sa = new SoftAssert();

	@Test(priority = 0)
	public void elementIsPresentHomePageGoogleTest() throws InterruptedException {

		// Validate Home page is landing
		Assert.assertEquals(driver.getTitle(), "Google", "TC-000 ERROR: THE HOME PAGE IS NOT LANDED");
		Reporter.log("TC-000 THE HOME PAGE IS LANDED", true);

		/*
		 * 1. The Google LOGO image is present.
		 */
		log.debug("The Google LOGO image is present");

		// The Google Home LOGO image is present
		sa.assertTrue(isElementPresent(By.xpath(OR.getProperty("logogooglehome_xpath"))),
				"TC-001_1 ERROR: LOGO IS NOT PRESENT");
		// The Google Home LOGO image is displayed
		sa.assertTrue(isdisplayed("logogooglehome_xpath"), "TC-001_2 ERROR: THE HOME PAGE LOGO IS NOT DISPLAYED");

		// Check if LOGO image is displayed by using JavascriptExecutor
		WebElement ImageFile = driver.findElement(By.xpath(OR.getProperty("logogooglehomeimg_xpath")));
		Boolean ImagePresent = (Boolean) ((JavascriptExecutor) driver).executeScript(
				"return arguments[0].complete && typeof arguments[0].naturalWidth != \"undefined\" && arguments[0].naturalWidth > 0",
				ImageFile);
		sa.assertTrue(ImagePresent, "TC-001_3 ERROR: GOOGLE HOME PAGE LOGO IMAGE IS NOT DISPLAYED");

		Reporter.log("TC-001 THE HOME PAGE LOGO is PRESENT and DISPLAYED", true);

		/*
		 * 2. The Search TEXT FIELD is present.
		 */
		log.debug("The Search TEXT FIELD is present");

		// The Google Home SEARCH TEXT FIELD is present
		sa.assertTrue(isElementPresent(By.name(OR.getProperty("textfieldgooglehome_name"))),
				"TC-002-1 ERROR: SEARCH TEXT FIELD IS NOT PRESENT");
		// The Google Home SEARCH TEXT FIELD is displayed
		sa.assertTrue(isdisplayed("textfieldgooglehome_name"),
				"TC-002_2 ERROR: THE HOME PAGE SEARCH TEXT FIELD IS NOT DISPLAYED");

		Reporter.log("TC-002 THE HOME PAGE SEARCH TEXT FIELD is PRESENT and DISPLAYED", true);

		/*
		 * 3. The Google Search button is present.
		 */
		log.debug("The Google Search button is present");

		// The Google Home SEARCH BUTTON is present
		sa.assertTrue(isElementPresent(By.xpath(OR.getProperty("googlehomesearchbutton_xpath"))),
				"TC-003_1 ERROR: SEARCH BUTTON IS NOT PRESENT");
		// The Google Search button is displayed
		sa.assertTrue(isdisplayed("googlehomesearchbutton_xpath"),
				"TC-003_2 ERROR: THE HOME PAGE SEARCH BUTTON IS NOT DISPLAYED");

		Reporter.log("TC-003 THE HOME PAGE SEARCH BUTTONE is PRESENT and DISPLAYED", true);

		sa.assertAll();

	}

	@Test(priority = 1)
	public void searchFuncitonHomePageGoogleTest() {

		/*
		 * 4. Search text may be entered into the Search text field (e.g. ‘True Fit’)
		 */
		log.debug("Search text may be entered into the Search text field (e.g. ‘True Fit’)");

		// Search text may be entered into the Search text field (e.g. ‘True Fit’)
		WebElement searchFieldInput = driver.findElement(By.name(OR.getProperty("textfieldgooglehome_name")));
		searchFieldInput.clear();
		searchFieldInput.sendKeys("True Fit");
		String val = searchFieldInput.getAttribute("value"); // get value attribute with getAttribute()
		sa.assertEquals(val, "True Fit", "ERROR: TC-004_1 THE SEARCH TEXT IS NOT ENTERED INTO SEARCH FIELD");

		Reporter.log("TC-004 THE SEARCH TEXT MAY BE ENTERED INTO SEARCH FIELD: " + val, true);

		/*
		 * 5. Clicking the ‘Google Search’ button with search text yields search
		 * results.
		 */
		log.debug("Clicking the ‘Google Search’ button with search text yields search results");

		// Clicking the ‘Google Search’ button with search text yields search results.
		submit("googlehomesearchbutton_xpath");
		sa.assertEquals(driver.getTitle(), "True Fit - Google Search",
				"TC-005_1 ERROR: THE SEARCH PAGE IS NOT PRESENT IN TITLE");
		sa.assertTrue(driver.getCurrentUrl().contains("q=True+Fit"),
				"TC-005_2 ERROR: THE SEARCH RESULT IS NOT CONTAIN SEARCH ITEM IN URL");

		Reporter.log("TC-005 CLICKING THE 'GOOGLE SEARCH' BUTTON WITH SEARCH TEXT YIELDS SEARCH RESULT ", true);

		driver.navigate().back();

		/*
		 * 6. Clicking the ‘Google Search’ button with no search text will not perform a
		 * search.
		 */
		log.debug("Clicking the ‘Google Search’ button with search text yields search results");

		// Clicking the ‘Google Search’ button with no search text will not perform a
		// search.
		submit("googlehomesearchbutton_xpath");
		sa.assertEquals(driver.getTitle(), "Google", "TC-006_1 ERROR: THE HOME PAGE IS NOT LANDED");

		Reporter.log("TC-006 CLICKING THE SEARCH BUTTON WITH NO SEARCH TEXT WILL NOT PERFORM A SEARCH", true);

		sa.assertAll();

	}

}
