package com.truefithometest.base;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;

import com.aventstack.extentreports.Status;
import com.truefithometest.listeners.CustomListeners;
import io.github.bonigarcia.wdm.WebDriverManager;

public class TestBase {

	public static WebDriver driver;
	public static Properties config = new Properties();
	public static Properties OR = new Properties();
	public static FileInputStream fis;
	public static Logger log = Logger.getLogger("devpinoyLogger"); // (TestBase.class);
	public static WebDriverWait wait;
	public static String browser;

	@BeforeSuite
	public void setUP() {

		if (driver == null) {

			try {
				fis = new FileInputStream(
						System.getProperty("user.dir") + "/src/test/resources/properties/Config.properties");
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			try {
				config.load(fis);
				log.debug("Config file loaded !!!");
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			try {
				fis = new FileInputStream(
						System.getProperty("user.dir") + "/src/test/resources/properties/OR.properties");
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			try {
				OR.load(fis);
				log.debug("OR file loaded !!!");
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}

		if (System.getenv("browser") != null && !System.getenv("browser").isEmpty()) {

			browser = System.getenv("browser");
		} else {

			browser = config.getProperty("browser");

		}

		config.setProperty("browser", browser);

		if (config.getProperty("browser").equals("firefox")) {

			/*
			 * System.setProperty("webdriver.gecko.driver", System.getProperty("user.dir") +
			 * "src/test/resources/properties/executables/geckodriver");
			 */
			WebDriverManager.firefoxdriver().setup();
			driver = new FirefoxDriver();
			log.debug("Firefox Launched !!!");

		} else if (config.getProperty("browser").equals("chrome")) {

			/*
			 * System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir")
			 * + "src/test/resources/properties/executables/chromedriver");
			 */
			// Not headless mode
//			WebDriverManager.chromedriver().setup();
//			driver = new ChromeDriver();

			// Headless mode
			WebDriverManager.chromedriver().setup();
			ChromeOptions options = new ChromeOptions();
			options.setHeadless(true);
			driver = new ChromeDriver(options);

			log.debug("Chrome Launched !!!");
		}

		driver.get(config.getProperty("testsiteurl"));
		log.debug("Navigated to : " + config.getProperty("testsiteurl"));
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(Integer.parseInt(config.getProperty("implicit.wait")),
				TimeUnit.SECONDS);
		wait = new WebDriverWait(driver, 5);
	}

	// Current methods is implementing in test cases by replacing
	// diver.find...
	public boolean isdisplayed(String locator) {

		if (locator.endsWith("_css")) {
			driver.findElement(By.cssSelector(OR.getProperty(locator))).isDisplayed();
		} else if (locator.endsWith("_xpath")) {
			driver.findElement(By.xpath(OR.getProperty(locator))).isDisplayed();
		} else if (locator.endsWith("_id")) {
			driver.findElement(By.id(OR.getProperty(locator))).isDisplayed();
		} else if (locator.endsWith("_name")) {
			driver.findElement(By.name(OR.getProperty(locator))).isDisplayed();
		}
		CustomListeners.testReport.get().log(Status.INFO, "isDisplayed on : " + locator);
		return true;
	}

// Current methods is implementing in test cases by replacing diver.find... 
	public void submit(String locator) {

		if (locator.endsWith("_css")) {
			driver.findElement(By.cssSelector(OR.getProperty(locator))).submit();
		} else if (locator.endsWith("_xpath")) {
			driver.findElement(By.xpath(OR.getProperty(locator))).submit();
		} else if (locator.endsWith("_id")) {
			driver.findElement(By.id(OR.getProperty(locator))).submit();
		} else if (locator.endsWith("_name")) {
			driver.findElement(By.name(OR.getProperty(locator))).submit();
		}
		CustomListeners.testReport.get().log(Status.INFO, "Submit on : " + locator);
	}

	// Current methods should be implementing in test cases by replacing
	// diver.find... - Pending
	public void type(String locator, String value) {

		if (locator.endsWith("_css")) {
			driver.findElement(By.cssSelector(OR.getProperty(locator))).clear();
			driver.findElement(By.cssSelector(OR.getProperty(locator))).sendKeys(value);
		} else if (locator.endsWith("_xpath")) {
			driver.findElement(By.xpath(OR.getProperty(locator))).clear();
			driver.findElement(By.xpath(OR.getProperty(locator))).sendKeys(value);
		} else if (locator.endsWith("_id")) {
			driver.findElement(By.id(OR.getProperty(locator))).clear();
			driver.findElement(By.id(OR.getProperty(locator))).sendKeys(value);
		} else if (locator.endsWith("_name")) {
			driver.findElement(By.name(OR.getProperty(locator))).clear();
			driver.findElement(By.name(OR.getProperty(locator))).sendKeys(value);
		}

		CustomListeners.testReport.get().log(Status.INFO, "Typing in : " + locator + " entered value as " + value);

	}

	public boolean isElementPresent(By by) {

		try {

			driver.findElement(by);
			return true;

		} catch (NoSuchElementException e) {

			return false;

		}

	}

	@AfterSuite
	public void tearDown() {

		if (driver != null) {
			driver.quit();
		}

		log.debug("Test execution completed !!!");

	}

}
